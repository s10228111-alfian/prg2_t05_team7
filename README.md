Things to work on:

- 1. [] Basic Requirements
     - [x] Base Code
     - [] Implementation
       - [x] General
       - [] Screening
         - [] Add a Movie Screening Session
         - [] Delete a Movie Screening Session
       - [] Order
         - [] Order Movie Tickets
         - [] Cancel Movie Tickets
- 2. [] Advance Features
     - [] Base Code
     - [] Implementation
- [] Exception Handling

Credits to:
[Git ignore template](https://github.com/github/gitignore/blob/main/VisualStudio.gitignore)
