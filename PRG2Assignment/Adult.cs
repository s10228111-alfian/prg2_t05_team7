﻿//============================================================
// Student Number : S10227870, S10228111
// Student Name : Natalie Tesia Koh, Muhammad Nuralfian Bin Abdul Rashid
// Module Group : T05
//============================================================
    
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    class Adult:Ticket
    {
        private bool popcornOffer;

        public bool PopcornOffer
        {
            get { return popcornOffer; }
            set { popcornOffer = value; }
        }
        public Adult(){}
        public Adult(Screening sc, bool po) : base(sc)
        {
            PopcornOffer = po;
        }
        public override double CalculatePrice()
        {
            if (SCreening.ScreeningType == "2D")
            {
                if (SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Friday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Saturday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Sunday)
                {
                    if (PopcornOffer)
                    {
                        return 12.50 + 3;
                    }
                    else
                    {
                        return 12.50;
                    }
                    
                }
                else
                {
                    if (PopcornOffer)
                    {
                        return 8.50 + 3;
                    }
                    else
                    {
                        return 8.50;
                    }
                    
                }
            }
            else
            {
                if (SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Friday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Saturday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Sunday)
                {
                    if (PopcornOffer)
                    {
                        return 14.00 + 3;
                    }
                    else
                    {
                        return 14.00;
                    }
                    
                }
                else
                {
                    if (PopcornOffer)
                    {
                        return 11.00 + 3;
                    }
                    else
                    {
                        return 11.00;
                    }
                    
                }
            }

        }
        public override string ToString()
        {
            return base.ToString() + "Popcorn Offer: " + PopcornOffer;
        }
    }
}
