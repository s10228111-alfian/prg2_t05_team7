﻿//============================================================
// Student Number : S10227870, S10228111
// Student Name : Natalie Tesia Koh, Muhammad Nuralfian Bin Abdul Rashid
// Module Group : T05
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    class Cinema
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private int hallNo;

        public int HallNo
        {
            get { return hallNo; }
            set { hallNo = value; }
        }

        private int capacity;

       public int Capacity
        {
            get { return capacity; }
            set { capacity = value; }
        }

        public Cinema() { }
        public Cinema(string n, int h, int c)
        {
            Name = n;
            HallNo = h;
            Capacity = c;
        }
        public override string ToString()
        {
            return "Name: " + Name + "Hall no: " + HallNo + "Capacity : " + Capacity;
        }

    }
}
