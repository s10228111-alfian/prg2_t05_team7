﻿//============================================================
// Student Number : S10227870, S10228111
// Student Name : Natalie Tesia Koh, Muhammad Nuralfian Bin Abdul Rashid
// Module Group : T05
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    class SeniorCitizen: Ticket
    {
        private int yearOfBirth;

        public int YearOfBirth
        {
            get { return yearOfBirth; }
            set { yearOfBirth = value; }
        }
        public SeniorCitizen() { }
        public SeniorCitizen(Screening sc, int yob) : base(sc)
        {
            YearOfBirth = yob;
        }
        public override double CalculatePrice()
        {
            DateTime opendate = SCreening.Movie.OpeningDate;
            DateTime datenow = SCreening.ScreeningDateTime;
            int days = datenow.Subtract(opendate).Days;
            if (days <= 7)
            {
                if (SCreening.ScreeningType == "2D")
                {
                    if (SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Friday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Saturday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Sunday)
                    {
                        return 12.50;
                    }
                    else
                    {
                        return 8.50;
                    }
                }
                else
                {
                    if (SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Friday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Saturday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Sunday)
                    {
                        return 14.00;
                    }
                    else
                    {
                        return 11.00;
                    }
                }
            }
            else if (SCreening.ScreeningType == "2D")
            {
                if (SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Friday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Saturday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Sunday)
                {
                    return 12.50;
                }
                else
                {
                    return 5.00;
                }
            }
            else
            {
                if (SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Friday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Saturday || SCreening.ScreeningDateTime.DayOfWeek == DayOfWeek.Sunday)
                {
                    return 14.00;
                }
                else
                {
                    return 6.00;
                }
            }
        }
        public override string ToString()
        {
            return base.ToString() + "Year of birth: " + YearOfBirth;
        }

    }
}
