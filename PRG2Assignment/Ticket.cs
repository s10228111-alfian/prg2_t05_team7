﻿//============================================================
// Student Number : S10227870, S10228111
// Student Name : Natalie Tesia Koh, Muhammad Nuralfian Bin Abdul Rashid
// Module Group : T05
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    abstract class Ticket 
    {
        private Screening screening;

        public Screening SCreening
        {
            get { return screening; }
            set { screening = value; }
        }
        public Ticket() { }
        public Ticket(Screening sc)
        {
            SCreening = sc;
        }
        public abstract double CalculatePrice();
        public override string ToString()
        {
            return "Screening: " + SCreening;
        }
    }
}
