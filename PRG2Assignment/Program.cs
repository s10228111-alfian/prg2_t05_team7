﻿//============================================================
// Student Number : S10227870, S10228111
// Student Name : Natalie Tesia Koh, Muhammad Nuralfian Bin Abdul Rashid
// Module Group : T05
//============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PRG2Assignment
{
    
    class Program
    {
        static void Main(string[] args)
        {


            List<Movie> m = new List<Movie>();
            List<Cinema> c = new List<Cinema>();
            List<Screening> sc = new List<Screening>();
            List<Order> o = new List<Order>();

            bool start = true;

            while (start)
            {
                if (sc.Count > 0)
                {
                    recommendation(o, m);
                    DisplayMenu();

                }
                else
                {
                    Console.WriteLine("Please load movie data then load screening data\n");
                    DisplayMenu();
                }
               
                string option = Console.ReadLine();
                
                if(option == "1")
                {
                    m = new List<Movie>();
                    c = new List<Cinema>();
                    LoadMovieData(m, c, sc);
                    Console.WriteLine("\nMovie data loaded!\n");
                }
                else if(option == "2")
                {
                    if(m.Count == 0)
                    {
                        Console.WriteLine("\nPlease load the Movie data first\n");
                    }
                    else
                    {
                        screeningNo = 1;
                        sc = new List<Screening>();
                        LoadScreeningData(sc, c, m);
                        loadOrders(o, m, sc);
                        m = new List<Movie>();
                        c = new List<Cinema>();
                        LoadMovieData(m, c, sc);
                        Console.WriteLine("\nScreening data loaded\n");
                    }
                    
                }
                else if (option == "3")
                {
                    if (sc.Count == 0)
                    {
                        Console.WriteLine("\nPlease load the screening data first\n");
                    }
                    else
                    {
                        DisplayMovie(m);
                    }
                }
                else if(option == "4")
                {
                    if (sc.Count == 0)
                    {
                        Console.WriteLine("\nPlease load the screening data first\n");
                    }
                    else
                    {
                        MoviesScreening(m,sc);
                    }
                }
                else if(option == "5")
                {
                    if (sc.Count == 0)
                    {
                        Console.WriteLine("\nPlease load the screening data first\n");
                    }
                    else
                    {
                        AddMovieScreening(m, c, sc);
                    }
                 }
                else if (option == "6")
                {
                    if (sc.Count == 0)
                    {
                        Console.WriteLine("\nPlease load the screening data first\n");
                    }
                    else
                    {
                        DeleteMovieScreening(sc, m, c);
                    }
                }
                else if(option == "7")
                {
                    if (sc.Count == 0)
                    {
                        Console.WriteLine("\nPlease load the screening data first\n");
                    }
                    else
                    {
                        AddOrders(m, o);
                    }
                }
                else if(option == "8")
                {
                    if (sc.Count == 0)
                    {
                        Console.WriteLine("\nPlease load the screening data first\n");
                    }
                    else
                    {
                        cancelTicket(o);
                    }
                }
                else if(option == "0")
                {
                    start = false;
                }
                else
                {
                    Console.WriteLine("\nSorry, option is unavailable please try again\n");
                }
            }
        }
        public static int orderNo = 1;
        public static int screeningNo = 1;
        public static void LoadMovieData(List<Movie> m, List<Cinema> c, List<Screening> sc)
        {
            /* this is to convert the csv file to data*/
            string[] csvlines = File.ReadAllLines("Movie.csv");
            
            /* This is to add into the movie list*/
            for (int i= 1; i<csvlines.Length; i++)
            {
                string[] lines = csvlines[i].Split(",");
                string[] genre = lines[2].Split("/");
                List<string> G = new List<string>();

                foreach(string word in genre)
                {
                    G.Add(word);
                }
             
                List<Screening> screenlist = new List<Screening>();
                foreach (Screening SCreen in sc)
                {
                    if (SCreen.Movie.Title == lines[0])
                    {
                        screenlist.Add(SCreen);
                    }
                }

                string date = Convert.ToString(lines[4]);
                DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", null);

                m.Add(new Movie(lines[0], Convert.ToInt32(lines[1]), lines[3], dt , G, screenlist));

            }
            string[] csv2lines = File.ReadAllLines("Cinema.csv");
           
            /* This is to add into the movie list*/
            for (int i = 1; i < csv2lines.Length; i++)
            {
                string[] lines = csv2lines[i].Split(",");
                c.Add(new Cinema(lines[0], Convert.ToInt32(lines[1]), Convert.ToInt32(lines[2])));
            }




        }
        public static void LoadScreeningData(List<Screening> sc, List<Cinema> c, List<Movie> m)
        {
            /* this is to convert the csv file to data*/
            string[] csvlines = File.ReadAllLines("Screening.csv");
            string[] title = csvlines[0].Split(",");
           
           
            /* This is to add into the movie list*/
            for (int i = 1; i < csvlines.Length; i++)
            {
                string[] lines = csvlines[i].Split(",");
                Cinema newCinema = null;
                foreach(Cinema Ci in c)
                {
                    if (Ci.Name == lines[2] && Ci.HallNo == Convert.ToInt32(lines[3]))
                    {
                        newCinema = Ci;
                    }
                }
                Movie newMovie = null;
                foreach (Movie Move in m)
                {
                    if (Move.Title == lines[4] )
                    {
                        newMovie = Move;
                    }
                }
                Screening s = new Screening(screeningNo, Convert.ToDateTime(lines[0]), lines[1], newCinema, newMovie);
                s.SeatsRemaining = s.Cinema.Capacity;
                sc.Add(s);
                newMovie.AddScreening(s);
                screeningNo += 1;
            }
            
            
        }
        public static void loadOrders(List<Order> o, List<Movie> m, List<Screening> sc)
        {
            int xy = 0;
            int num = 5;
            for (int i=0; i<5; i++)
            { 
                List<Ticket> t = new List<Ticket>();
                for (int x = 0; x < num; x++)
                {
                    Ticket ti = new Adult(sc[xy], true);
                    t.Add(ti);
                    ti.SCreening.SeatsRemaining -= 1;
                }
                num -= 1;
                xy += 1;

                o.Add(new Order(orderNo, DateTime.Now));
                o[i].TicketList = t;
            }
        }

        public static void DisplayMenu()
        {
            Console.WriteLine("1. Load Movie data");
            Console.WriteLine("2. Load Screening data");
            Console.WriteLine("3. List all movies");
            Console.WriteLine("4. List movie screening");
            Console.WriteLine("5. Add movie screening");
            Console.WriteLine("6. Delete movie screening");
            Console.WriteLine("7. Order tickets");
            Console.WriteLine("8. Cancel order");
            Console.WriteLine("0. Exit \n");

            Console.Write("Enter your option: ");
        }

        public static void DisplayMovie(List<Movie> m)
        {
            Console.WriteLine();
            Console.WriteLine("{0,-30}{1,-30}{2,-30}{3,-30}{4,-30}", "Title", "Duration", "Classification", "Genre", "Opening Date");
            foreach (Movie moo in m)
            {
                string genre = "";
                foreach(string s in moo.GenreList)
                {
                    genre += "/"+s;
                }
                Console.WriteLine("{0,-30}{1,-30}{2,-30}{3,-30}{4,-30}", moo.Title, moo.Duration, moo.Classification, genre, moo.OpeningDate);
            }
            Console.WriteLine();
        }

        public static void DisplayCinema(List<Cinema> c)
        {

            IEnumerable<Cinema> cinemalist = c.OrderByDescending(Cinema => Cinema.Capacity);

            Console.WriteLine();
            Console.WriteLine("{0,-30}{1,-30}{2,-30}", "Cinema Name", "Cinema HallNo", "Cinema Capacity");
            foreach (Cinema Cine in cinemalist)
            {
                Console.WriteLine("{0,-30}{1,-30}{2,-30}", Cine.Name, Cine.HallNo, Cine.Capacity);
            }
            Console.WriteLine();
        }

        public static void DisplayScreening(List<Screening> screen)
        {
            Console.WriteLine();
            Console.WriteLine("{0,-20}{1,-30}{2,-30}{3,-30}{4,-20}{5,-20}", "Screening No", "Date Time", "Screening Type", "Hall  No", "Cinema Name", "Capacity");

            IEnumerable<Screening> screenlist = screen.OrderByDescending(Screening => Screening.SeatsRemaining);

            foreach (Screening sc in screenlist)
            {
                Console.WriteLine("{0,-20}{1,-30}{2,-30}{3,-30}{4,-20}{5,-20}", sc.ScreeningNo, sc.ScreeningDateTime, sc.ScreeningType, sc.Cinema.HallNo, sc.Cinema.Name, sc.Cinema.Capacity);
            }
            Console.WriteLine();
        }

        public static void MoviesScreening(List<Movie> m, List<Screening> scl)
        {
            bool flag = false;
            bool start = true;

            DisplayMovie(m);

            while (start == true)
            {
                Console.Write("Enter a Movie title: ");
                string movieTitle = Console.ReadLine();
                Movie chosenMovie = null;

                foreach (Movie moo in m)
                {
                    if (moo.Title == movieTitle)
                    {
                        chosenMovie = moo;
                        flag = false;
                        break;
                    }
                    else
                    {
                        flag = true;
                    }
                }
                if (flag == true)
                {
                    Console.WriteLine("Invalid Movie Title. Try Again");
                    start = true;
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("{0,-20}{1,-25}{2,-20}{3,-20}{4,-20}{5,-20}", "Screening No", "Date Time", "Screening Type", "Hall  No", "Cinema Name", "Capacity");

                    IEnumerable<Screening> screenlist = scl.OrderByDescending(Screening => Screening.SeatsRemaining);

                    foreach (Screening sc in screenlist)
                    {
                        if (sc.Movie.Title == chosenMovie.Title)
                        { 
                            Console.WriteLine("{0,-20}{1,-25}{2,-20}{3,-20}{4, -20}{5,-20}", sc.ScreeningNo, sc.ScreeningDateTime, sc.ScreeningType, sc.Cinema.HallNo, sc.Cinema.Name, sc.Cinema.Capacity);
                        }
                    }
                    Console.WriteLine();
                    start = false;
                }
            }

            
        }

        public static void AddMovieScreening(List<Movie> m, List<Cinema> c, List<Screening> sc)
        {
            bool start = true;
            bool start2 = true;
            bool start3 = true;
            bool start4 = true;
            bool start5 = true;
            bool end = false;

            string movie = "";
            string screentype = "";
            DateTime datesel = DateTime.Now;
            int cleaningtime = 30;
            string selcinemaname = "";
            int selcinemahall = 0;

            // List all movies
            DisplayMovie(m);

            while (start)
            {
                bool flag = true;
                // Prompt user to select a movie
                Console.Write("Enter a Movie Title (e.g Sing 2): ");
                string selMovie = Console.ReadLine();

                foreach (Movie moo in m)
                {
                    if (moo.Title == selMovie)
                    {
                        flag = false;
                        movie = selMovie;
                        break;
                    }
                    else
                    {
                        flag = true;
                    }
                }

                if (flag)
                {
                    Console.WriteLine("Invalid Movie Title. Try Again");
                } else
                {
                    break;
                }
            }
            while (start2)
            {
                // Prompt user to enter a screening type
                Console.Write("Enter a Screening Type (e.g 2D/3D): ");
                string screenType = Console.ReadLine();
                    
                if (screenType.ToUpper() == "2D" || screenType.ToUpper() == "3D")
                {
                    screentype = screenType;
                    break;
                } else
                {
                    Console.WriteLine("Enter a valid Screen Type (e.g 2D / 3D)");
                    start2 = true;
                }

            }
            while (start3)
            {

                bool flag = true;

                // Prompt user to enter a screening date and time
                Console.Write("Enter a Screening date and time(e.g 13/01/2022 8:00PM): ");
                string newdate = Console.ReadLine();
                if (DateTime.TryParse(newdate, out datesel))
                {
                    foreach (Movie mov in m)
                    {
                        if (movie.ToUpper() == mov.Title.ToUpper())
                        {
                            if (datesel > mov.OpeningDate)
                            {
                                datesel = Convert.ToDateTime(newdate);
                                flag = false;
                                break;
                            }
                        }
                    }
                    if (flag)
                    {
                        Console.WriteLine("Invalid screening date.The new screening date should be later than opening date.");
                    }
                    else
                    {
                        break;
                    }

                }
                else
                {
                    Console.WriteLine("Enter a valid date");
                    start3 = true;
                }
            }

            foreach (Movie mov in m)
            {
                if (movie.ToUpper() == mov.Title.ToUpper())
                {
                    if (datesel.Date.DayOfWeek >= mov.OpeningDate.Date.DayOfWeek && datesel.TimeOfDay.TotalMilliseconds >= mov.OpeningDate.TimeOfDay.TotalMilliseconds)
                    {
                        bool flag = true;
                        // List all cinema halls
                        DisplayCinema(c);

                        while (start4)
                        {
                            // Prompt user to select a cinema hall
                            Console.Write("Select Cinema Name: ");
                            string cinemaname = Console.ReadLine();

                            foreach (Cinema cinema in c)
                            {
                                if (cinemaname.ToUpper() == cinema.Name.ToUpper())
                                {
                                    flag = false;
                                    selcinemaname = cinemaname;
                                    break;
                                }
                                else
                                {
                                    flag = true;
                                }
                            }
                            if (flag)
                            {
                                Console.WriteLine("Invalid Cinema Name");
                            }
                            else
                            {
                                break;
                            }
                        }

                        while (start5)
                        {
                            Console.Write("Select Cinema Hall: ");
                            string cinemaHall = Console.ReadLine();
                            foreach (Cinema cinema in c)
                            {
                                if (cinemaHall == Convert.ToString(cinema.HallNo))
                                {
                                    flag = false;
                                    selcinemahall = Convert.ToInt32(cinemaHall);
                                    break;
                                }
                                else
                                {
                                    flag = true;
                                }
                            }
                            if (flag)
                            {
                                Console.WriteLine("Invalid Cinema Hall No");
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        start3 = true;
                        break;
                    }
                }
                    
            }
            
            // Check to ensure movie duration and cleaning time is considered before adding a screen session
            foreach (Screening screen in sc)
            {
                if (screen.Movie.Title == movie)
                {
                    int maxtime = screen.Movie.Duration + cleaningtime;
                    TimeSpan timeconvert = TimeSpan.FromMinutes(maxtime);
                    //DateTime timeconvert = DateTime.Parse(Convert.ToString(maxtime));
                    Cinema insertCine = null;
                    if (screen.ScreeningDateTime.Date == datesel.Date)
                    {
                        if (screen.ScreeningDateTime.AddMinutes(maxtime).Date <= datesel.Date)
                        {
                            // Create Screening Object with given info and to relevant screening list
                            insertCine = new Cinema(screen.Cinema.Name, screen.Cinema.HallNo, screen.Cinema.Capacity);
                            Screening newScreeningSD = new Screening(screeningNo, datesel, screentype, insertCine, screen.Movie);
                            int check = screen.ScreeningDateTime.AddMinutes(maxtime).CompareTo(newScreeningSD.ScreeningDateTime);
                            if (screen.ScreeningDateTime == newScreeningSD.ScreeningDateTime)
                            {
                                Console.WriteLine("Screening is occupied at the cinema and hall no requested.");
                                movie = "";
                                screentype = "";
                                datesel = DateTime.Now;
                                cleaningtime = 30;
                                selcinemaname = "";
                                selcinemahall = 0;
                                start = true;
                                start2 = true;
                                start3 = true;
                                start4 = true;
                                start5 = true;
                                end = false;
                                break;
                            } else if (check > 0)
                            {
                                Console.WriteLine("Screening is occupied at the cinema and hall no requested.");
                                movie = "";
                                screentype = "";
                                datesel = DateTime.Now;
                                cleaningtime = 30;
                                selcinemaname = "";
                                selcinemahall = 0;
                                start = true;
                                start2 = true;
                                start3 = true;
                                start4 = true;
                                start5 = true;
                                end = false;
                                break;
                            }
                            else
                            {
                                screen.Movie.AddScreening(newScreeningSD);
                                sc.Add(newScreeningSD);
                                screeningNo++;
                            }

                            // Display Status of the movie screening session creation (Sucessful or Unsucessful)
                            Console.WriteLine("{0} has successfully been added to {1} in Hall {2} at {3}.", screen.Movie.Title, screen.Cinema.Name, screen.Cinema.HallNo, datesel);
                            end = true;
                            movie = "";
                            screentype = "";
                            datesel = DateTime.Now;
                            cleaningtime = 30;
                            selcinemaname = "";
                            selcinemahall = 0;
                            start = true;
                            start2 = true;
                            start3 = true;
                            start4 = true;
                            start5 = true;
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Invalid Booking. Screening slot is occupied.");
                            movie = "";
                            screentype = "";
                            datesel = DateTime.Now;
                            cleaningtime = 30;
                            selcinemaname = "";
                            selcinemahall = 0;
                            start = true;
                            start2 = true;
                            start3 = true;
                            start4 = true;
                            start5 = true;
                            end = false;
                        }
                    } else
                    {
                        // Create Screening Object with given info and to relevant screening list
                        insertCine = new Cinema(screen.Cinema.Name, screen.Cinema.HallNo, screen.Cinema.Capacity);
                        Screening newScreeningDT = new Screening(screeningNo, datesel, screentype, insertCine, screen.Movie);
                        screen.Movie.AddScreening(newScreeningDT);
                        sc.Add(newScreeningDT);
                        screeningNo++;

                        // Display Status of the movie screening session creation (Sucessful or Unsucessful)
                        Console.WriteLine("{0} has successfully been added to {1} in Hall {2} at {3}.", screen.Movie.Title, screen.Cinema.Name, screen.Cinema.HallNo, datesel);
                        end = true;
                        movie = "";
                        screentype = "";
                        datesel = DateTime.Now;
                        cleaningtime = 30;
                        selcinemaname = "";
                        selcinemahall = 0;
                        start = true;
                        start2 = true;
                        start3 = true;
                        start4 = true;
                        start5 = true;
                        break;
                    }
                }
            }
            if (end)
            {
                Console.WriteLine("Successful Add Screening\n");

            }
            else
            {
                Console.WriteLine("Unsucessful Add Screening\n");
                movie = "";
                screentype = "";
                datesel = DateTime.Now;
                cleaningtime = 30;
                selcinemaname = "";
                selcinemahall = 0;
                start = true;
                start2 = true;
                start3 = true;
                start4 = true;
                start5 = true;
                end = false;
            }
        }
           
        public static void DeleteMovieScreening(List<Screening> sc, List<Movie> m, List<Cinema> c)
        {

            bool flag = true;

            IEnumerable<Screening> screenlist = sc.OrderByDescending(Screening => Screening.SeatsRemaining);

            Console.WriteLine("{0,-20}{1,-30}{2,-30}{3,-30}{4,-20}{5,-20}", "Screening No", "Date Time", "Screening Type", "Hall  No", "Cinema Name", "Capacity");

            foreach (Screening screen in screenlist)
            {
                if(screen.SeatsRemaining == screen.Cinema.Capacity)
                {
                    Console.WriteLine("{0,-20}{1,-30}{2,-30}{3,-30}{4,-20}{5,-20}", screen.ScreeningNo, screen.ScreeningDateTime, screen.ScreeningType, screen.Cinema.HallNo, screen.Cinema.Name, screen.Cinema.Capacity);
                }
            }

            while (true)
            {
                Console.Write("\nSelect a Session No (enter 'x' to exit): ");
                string delSession = Console.ReadLine();
                Screening delscreen = null;

                if (delSession.ToLower() == "x"){
                    Console.Write("Are you sure to stop this operation? [Y/N] ");
                    string confirm = Console.ReadLine();
                    if (confirm.ToLower() == "y")
                    {
                        break;
                    }
                }

                foreach (Screening screening in sc)
                {
                    if (Convert.ToString(screening.ScreeningNo) == Convert.ToString(delSession))
                    {
                        delscreen = screening;

                        if (delscreen.SeatsRemaining == delscreen.Cinema.Capacity)
                        {
                            screening.Movie.ScreeningList.Remove(screening);
                            sc.Remove(screening);
                            Console.WriteLine("Screening No {0} has been removed.", delSession);
                            flag = false;
                            break;
                        }
                    }
                }
                if (!flag)
                {
                    flag = true;
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid screen no");

                }



            }

            DisplayScreening(sc);

        }

        public static void AddOrders(List<Movie> m, List<Order> o)
        {
            DisplayMovie(m);
            Console.WriteLine("Enter a Movie title(press any numeral to quit): ");
            string movieTitle = Console.ReadLine();
            Movie chosenMovie = null;

            foreach (Movie moo in m)
            {
                if (moo.Title.ToLower() == movieTitle.ToLower())
                {
                    chosenMovie = moo;

                }
            }
            if (chosenMovie == null)
            {
                Console.WriteLine("Enter movie title wrongly, plese try again");
            }
            else
            {
                Console.WriteLine("{0,-10} {1,-25} {2,-25} {3,-30} {4,-20} {5,-20} {6,-20}", "Screening No", "Date Time", "Screening Type", "Hall  No", "Cinema Name", "Seats remaining", "Cinema Capacity");
                foreach (Screening sc in chosenMovie.ScreeningList)
                {

                    Console.WriteLine("{0,-10} {1,-25} {2,-25} {3,-30} {4,-20} {5,-20} {6,-25}", sc.ScreeningNo, sc.ScreeningDateTime, sc.ScreeningType, sc.Cinema.HallNo, sc.Cinema.Name, sc.SeatsRemaining, sc.Cinema.Capacity);
                }
                Console.WriteLine("Enter Quantity of Tickets: ");
                string qtywords = Console.ReadLine();
                int qty = 0;
                bool intornot = int.TryParse(qtywords, out qty);

                if (intornot == false)
                {
                    Console.WriteLine("Please enter a number value");
                    AddOrders(m, o);
                }
                else
                {

                
                qty = Convert.ToInt32(qtywords);
                Console.WriteLine("Enter Screening No: ");
                    string scnowords = Console.ReadLine();
                    int scrno = 0;
                bool intornot2 = int.TryParse(scnowords, out scrno);

                    if (intornot2 == false)
                    {
                        Console.WriteLine("Please enter a Screening no");
                        AddOrders(m, o);
                    }
                    else
                    {
                    int screenNo = Convert.ToInt32(scnowords);
                    Screening chosenScreening = null;
                List<int> ageList = new List<int>();
                foreach (Screening sc in chosenMovie.ScreeningList)
                {
                    if (sc.ScreeningNo == screenNo)
                    {
                        
                        chosenScreening = sc;

                        
                    }

                }
                        if (chosenScreening == null) {
                            Console.WriteLine("Please enter the proper screening number");
                            AddOrders(m, o);
                        }
                        else { 
                            if (chosenScreening.SeatsRemaining < qty)
                            {
                                Console.WriteLine("Not enough capacity! choose a lesser number");
                                AddOrders(m, o);
                                qty = 0;
                            }



                            else
                            {
                                if (qty == 0)
                                {
                                    Console.WriteLine("Please enter another number for tickets");
                                }
                                orderNo += 1;
                                /* this is to check the age*/
                                bool agecorrect = false;
                                bool g = false;
                                bool check = true;
                                for (int y = 0; y < qty; y++)
                                {
                                    if (check == true)
                                    {

                                        if (chosenMovie.Classification == "PG13")
                                        {
                                            Console.WriteLine("Enter Year of Birth('yyyy/mm/dd') Please do not put any invalid date");
                                            
                                            DateTime dob = Convert.ToDateTime(Console.ReadLine());
                                            
                                                int ageDiff = (DateTime.Now.Year) - (dob.Year);
                                            if (ageDiff > 13)
                                            {
                                                agecorrect = true;
                                                ageList.Add(ageDiff);
                                            }
                                            else
                                            {

                                                Console.WriteLine("Your Age is not suited for the show, Do you want to enter again? [Y/N]:");
                                                string ans = Console.ReadLine();
                                                if (ans == "Y")
                                                {
                                                    y--;
                                                }
                                                else
                                                {

                                                    Console.WriteLine("Cancelling your ticket");
                                                    check = false;
                                                    break;
                                                }

                                            }


                                        }
                                        else if (chosenMovie.Classification == "NC16")
                                        {
                                            Console.WriteLine("Enter Year of Birth('yyyy/mm/dd')Please do not put any invalid date");
                                            DateTime dob = Convert.ToDateTime(Console.ReadLine());
                                            int ageDiff = (DateTime.Now.Year) - (dob.Year);
                                            if (ageDiff > 16)
                                            {
                                                agecorrect = true;
                                                ageList.Add(ageDiff);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Your Age is not suited for the show, Do you want to enter again? [Y/N]:");
                                                string ans = Console.ReadLine();
                                                if (ans == "Y")
                                                {
                                                    y--;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Cancelling your ticket");
                                                    check = false;
                                                    break;
                                                }
                                            }
                                        }
                                        else if (chosenMovie.Classification == "M18")
                                        {
                                            Console.WriteLine("Enter Year of Birth('yyyy/mm/dd')Please do not put any invalid date");
                                            DateTime dob = Convert.ToDateTime(Console.ReadLine());
                                            int ageDiff = (DateTime.Now.Year) - (dob.Year);
                                            if (ageDiff > 18)
                                            {
                                                agecorrect = true;
                                                ageList.Add(ageDiff);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Your Age is not suited for the show, Do you want to enter again? [Y/N]:");
                                                string ans = Console.ReadLine();
                                                if (ans == "Y")
                                                {
                                                    y--;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Cancelling your ticket");
                                                    check = false;
                                                    break;
                                                }
                                            }
                                        }
                                        else if (chosenMovie.Classification == "R21")
                                        {
                                            Console.WriteLine("Enter Year of Birth('yyyy/mm/dd')Please do not put any invalid date");
                                            DateTime dob = Convert.ToDateTime(Console.ReadLine());
                                            int ageDiff = (DateTime.Now.Year) - (dob.Year);
                                            if (ageDiff > 21)
                                            {
                                                agecorrect = true;
                                                ageList.Add(ageDiff);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Your Age is not suited for the show, Do you want to enter again? [Y/N]:");
                                                string ans = Console.ReadLine();
                                                if (ans.ToUpper() == "Y")
                                                {
                                                    y--;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Cancelling your ticket");
                                                    check = false;
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            agecorrect = true;
                                            g = true;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Have A nice day");
                                        break;
                                    }



                                }
                                if (agecorrect == true)
                                {
                                    bool orderstatus = false;
                                    Order or = new Order(orderNo, DateTime.Now);
                                    or.Status = "Unpaid";
                                    o.Add(or);

                                    List<Ticket> ticketList = new List<Ticket>();
                                    double amt = 0.0;
                                   
                                    for (int y = 0; y < qty; y++)
                                    {
                                        
                                        Console.WriteLine("Enter Ticket Type (Student, Senior Citizen, Adult)");
                                        string type = Console.ReadLine().ToLower();

                                        if (type == "student" && g == false)
                                        {
                                            Console.WriteLine("Enter Level of study(Primary, secondary, Tietiary):");
                                            string los = Console.ReadLine().ToLower();
                                            /*Student s = new Student(chosenScreening, los);
                                            ticketList.Add(s);
                                            amt += s.CalculatePrice();*/
                                            if (los == "primary" && ageList[y] < 13 )
                                            {
                                                Student s = new Student(chosenScreening, los);
                                                ticketList.Add(s);
                                                or.TicketList = ticketList;
                                                amt += s.CalculatePrice();
                                                chosenScreening.SeatsRemaining -= 1;
                                            }
                                            else if (los == "secondary" && ageList[y] < 16 )
                                            {
                                                Student s = new Student(chosenScreening, los);
                                                ticketList.Add(s);
                                                or.TicketList = ticketList;
                                                amt += s.CalculatePrice();
                                                chosenScreening.SeatsRemaining -= 1;

                                            }
                                            else if (los == "tertiary" && ageList[y] < 18 )
                                            {
                                                Student s = new Student(chosenScreening, los);
                                                ticketList.Add(s);
                                                or.TicketList = ticketList;
                                                amt += s.CalculatePrice();
                                                chosenScreening.SeatsRemaining -= 1;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Sorry, the ticket type you have input is not available for your age. Please choose another ticket type");
                                                y--;
                                            }




                                        }
                                        else if (type == "student" && g == true)
                                        {
                                            
                                            Console.WriteLine("Enter Level of study(Primary, secondary, Tietiary):");
                                            string los = Console.ReadLine().ToLower();
                                            /*Student s = new Student(chosenScreening, los);
                                            ticketList.Add(s);
                                            amt += s.CalculatePrice();*/
                                            Console.WriteLine("Enter your age(yyyy/mm/dd)Please do not put any invalid date: ");
                                            DateTime age = Convert.ToDateTime(Console.ReadLine());
                                            int ageDiff = (DateTime.Now.Year) - (age.Year);
                                            if (los == "primary" && ageDiff < 13)
                                            {
                                                Student s = new Student(chosenScreening, los);
                                                ticketList.Add(s);
                                                or.TicketList = ticketList;
                                                amt += s.CalculatePrice();
                                                chosenScreening.SeatsRemaining -= 1;
                                            }
                                            else if (los == "secondary" && ageDiff < 16)
                                            {
                                                Student s = new Student(chosenScreening, los);
                                                ticketList.Add(s);
                                                or.TicketList = ticketList;
                                                amt += s.CalculatePrice();
                                                chosenScreening.SeatsRemaining -= 1;

                                            }
                                            else if (los == "tietiary" && ageDiff < 18)
                                            {
                                                Student s = new Student(chosenScreening, los);
                                                ticketList.Add(s);
                                                or.TicketList = ticketList;
                                                amt += s.CalculatePrice();
                                                chosenScreening.SeatsRemaining -= 1;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Sorry, the ticket type you have input is not available for your age. Please choose another ticket type");
                                                y--;
                                            }




                                        }
                                        else if (type == "senior citizen" && g == false)
                                        {

                                            if (ageList[y] > 55)
                                            {
                                                Console.WriteLine("Enter year of birth: ");
                                                int yob = Convert.ToInt32(Console.ReadLine());
                                                SeniorCitizen sc = new SeniorCitizen(chosenScreening, yob);
                                                int yobdiff = Convert.ToInt32(DateTime.Now.Year) - yob;
                                                if (yobdiff > 55)
                                                {
                                                    ticketList.Add(sc);
                                                    or.TicketList = ticketList;
                                                    amt += sc.CalculatePrice();
                                                    chosenScreening.SeatsRemaining -= 1;
                                                }
                                                
                                            }
                                            else
                                            {
                                                Console.WriteLine("Sorry, the ticket type you have input is not available for your age. Please choose another ticket type");
                                                y--;
                                            }
                                        }
                                        else if (type == "senior citizen" && g == true)
                                        {
                                            Console.WriteLine("Enter year of birth: ");
                                            int yob = Convert.ToInt32(Console.ReadLine());
                                            
                                            int yobdiff = Convert.ToInt32(DateTime.Now.Year) - yob;
                                            if (yobdiff > 55)
                                            {
                                                SeniorCitizen sc = new SeniorCitizen(chosenScreening, yob);
                                                ticketList.Add(sc);
                                                or.TicketList = ticketList;
                                                amt += sc.CalculatePrice();
                                                chosenScreening.SeatsRemaining -= 1;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Sorry, the ticket type you have input is not available for your age. Please choose another ticket type");
                                                y--;
                                            }
                                        }
                                        else if (type == "adult")
                                        {
                                            Console.WriteLine("Do you want the popcorn set? [Y/N]: ");
                                            string ans = Console.ReadLine().ToLower();
                                            if (ans == "y")
                                            {
                                                Adult a = new Adult(chosenScreening, true);
                                                ticketList.Add(a);
                                                or.TicketList = ticketList;
                                                amt += a.CalculatePrice();
                                                chosenScreening.SeatsRemaining -= 1;
                                            }
                                            else
                                            {
                                                Adult a = new Adult(chosenScreening, false);
                                                ticketList.Add(a);
                                                or.TicketList = ticketList;
                                                amt += a.CalculatePrice();
                                                chosenScreening.SeatsRemaining -= 1;
                                            }


                                        }
                                        else
                                        {
                                            
                                            Console.WriteLine("You have Not input any ticket type, Do you Wish to cancel?[Y/N] ");
                                            string cancel = Console.ReadLine().ToLower();
                                            if (cancel == "y")
                                            {
                                                orderstatus = true;
                                                break;
                                            }
                                            else
                                            {
                                                
                                                y--;
                                            }
                                        }


                                    }
                                    if(orderstatus == true)
                                    {
                                        orderNo -= 1;
                                        chosenScreening.SeatsRemaining += qty;
                                        or.Status = "Cancelled";
                                        Console.WriteLine("Order is cancelled");
                                        Console.WriteLine("Press any key to confirm cancellation: ");
                                        string payment = Console.ReadLine();
                                    }
                                    else
                                    {
                                        Console.WriteLine("Confirm order [Y/N]: ");
                                        string confirmation = Console.ReadLine();
                                        if (confirmation.ToLower() == "y")
                                        {
                                            or.Status = "Paid";
                                            or.Amount = amt;

                                            Console.WriteLine("Total amount for ticket(s) is : " + or.Amount);

                                            Console.WriteLine("Order No: " + orderNo);
                                            Console.WriteLine("Press any key to make payment: ");
                                            string payment = Console.ReadLine();
                                        }
                                        else
                                        {
                                            orderNo -= 1;
                                            chosenScreening.SeatsRemaining += qty;
                                            or.Status = "Cancelled";
                                            Console.WriteLine("Order is cancelled");
                                            Console.WriteLine("Press any key to confirm cancellation: ");
                                            string payment = Console.ReadLine();
                                        }
                                    }
                                    


                                }
                            }
                        }
                }



                }
            }
            
            


        }

        public static void cancelTicket(List<Order> o)
        {
           
            if(o.Count == 0)
            {
                Console.WriteLine("There is no orders yet");
            }
            else
            {
                Console.WriteLine("Enter order Number: ");
                int userorder = Convert.ToInt32(Console.ReadLine());
                Order or = null;
                bool orderFound = false;
                foreach (Order xy in o)
                {
                    if (xy.OrderNo == userorder)
                    {
                        orderFound = true;
                        or = xy;
                        
                    }
                }
                if(orderFound == true)
                {
                    if (DateTime.Compare(or.TicketList[0].SCreening.ScreeningDateTime, DateTime.Now) < 0)
                    {
                        Console.WriteLine("Unable to cancel ticket");
                    }
                    else
                    {
                        
                        int qty = or.TicketList.Count;
                        Console.WriteLine(qty);
                        or.TicketList[0].SCreening.SeatsRemaining += qty;
                        or.Status = "Cancelled";
                        Console.WriteLine("Amount is refunded");
                        Console.WriteLine("Ticket Status: " + or.Status);
                    }
                }
                else
                {
                    Console.WriteLine("There is no such order made.");
                }
                
            }
           
           
            
        }

        public static void recommendation(List<Order> or, List<Movie> m)
        {
            
            if (or.Count == 0)
            {
                Console.WriteLine("There is no orders yet\n");
            }
            else
            {
                int x = 0;
                List<string> movieTitle = new List<string>();
                List<int> numofTicket = new List<int>();
                foreach (Order o in or)
                {
                    
                    if (o.TicketList == null)
                    {
                        continue;
                    }
                    else
                    {
                        
                        numofTicket.Add(o.TicketList.Count);
                        movieTitle.Add(o.TicketList[0].SCreening.Movie.Title);
                        x++;




                    }
                    

                }
                string recommend = "";
                string popmovie = "";
                List<string> popmovies = new List<string>();
                int count = 0;
                List<int> counts = new List<int>();
                for(int p = 0; p<movieTitle.Count; p++)
                {
                    popmovie = "";
                    count = 0;
                    for(int c = 0; c<movieTitle.Count; c++)
                    {
                        if(movieTitle[c] == movieTitle[p])
                        {

                            
                            count += numofTicket[c];
                            
                            popmovie = movieTitle[c];
                            
                        }

                        
                    }
                    popmovies.Add(popmovie);
                    counts.Add(count);
                }
                
                int max = counts[0];
                for (int i = 0; i < counts.Count; i++)
                {
                    if (counts[i] >= max)
                    {
                        max = counts[i];
                        recommend = popmovies[i];
                       
                    }
                }
                Console.WriteLine("Recommended movie: " + recommend + "\n");

            }
        }

    }
}
