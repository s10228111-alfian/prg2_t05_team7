﻿//============================================================
// Student Number : S10227870, S10228111
// Student Name : Natalie Tesia Koh, Muhammad Nuralfian Bin Abdul Rashid
// Module Group : T05
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    class Order
    {
        private int orderNo;

        public int OrderNo
        {
            get { return orderNo; }
            set { orderNo = value; }
        }
        private  DateTime orderDateTime;

        public  DateTime OrderDateTime
        {
            get { return orderDateTime; }
            set { orderDateTime = value; }
        }
        private double amount;

        public double Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        private string status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        private List<Ticket> ticketList;

        public List<Ticket> TicketList
        {
            get { return ticketList; }
            set { ticketList = value; }
        }

        public Order() { }
        public Order(int on, DateTime odt)
        {
            OrderNo = on;
            OrderDateTime = odt;
        }
        public void AddTicket(Ticket t)
        {
            ticketList.Add(t);
        }
   
        public override string ToString()
        {
            return "Order No: " + OrderNo + "Order Date Time: " + OrderDateTime + "Amount: " + Amount + "Status: " + Status + "Ticket List: " + TicketList;
        }
    }
}
