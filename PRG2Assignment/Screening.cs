﻿//============================================================
// Student Number : S10227870, S10228111
// Student Name : Natalie Tesia Koh, Muhammad Nuralfian Bin Abdul Rashid
// Module Group : T05
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    class Screening
    {
        private int screeningNo;

        public int ScreeningNo
        {
            get { return screeningNo; }
            set { screeningNo = value; }
        }

        private DateTime screeningDateTime;

        public DateTime ScreeningDateTime
        {
            get { return screeningDateTime; }
            set { screeningDateTime = value; }
        }

        private string screeningType;

        public string ScreeningType
        {
            get { return screeningType; }
            set { screeningType = value; }
        }

        private int seatsRemaining;

        public int SeatsRemaining
        {
            get { return seatsRemaining; }
            set { seatsRemaining = value; }
        }

        private Cinema cinema;

        public Cinema Cinema
        {
            get { return cinema; }
            set { cinema = value; }
        }

        private Movie movie;

        public Movie Movie
        {
            get { return movie; }
            set { movie = value; }
        }

        public Screening() { }

        public Screening(int sN, DateTime sDT, string sT, Cinema c, Movie m)
        {
            ScreeningNo = sN;
            ScreeningDateTime = sDT;
            ScreeningType = sT;
            Cinema = c;
            Movie = m;
            SeatsRemaining = c.Capacity;
        }

        public override string ToString()
        {
            return "ScreeningNo: " + ScreeningNo + "\tScreening DateTime: " + ScreeningDateTime + "\tScreening Type: " + ScreeningType + "\tCinema: " + Cinema + "\tMovie: " + Movie;
        }

    }
}
