﻿//============================================================
// Student Number : S10227870, S10228111
// Student Name : Natalie Tesia Koh, Muhammad Nuralfian Bin Abdul Rashid
// Module Group : T05
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2Assignment
{
    class Movie
    {
        private string title;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private int duration;

        public int Duration
        {
            get { return duration; }
            set { duration = value; }
        }

        private string classification;

        public string Classification
        {
            get { return classification; }
            set { classification = value; }
        }

        private DateTime openingDate;

        public DateTime OpeningDate
        {
            get { return openingDate; }
            set { openingDate = value; }
        }

        private List<string> genreList;

        public List<string> GenreList
        {
            get { return genreList; }
            set { genreList = value; }
        }

        private List<Screening> screeningList;

        public List<Screening> ScreeningList
        {
            get { return screeningList; }
            set { screeningList = value; }
        }

        public Movie() { }

        public Movie(string t, int d, string c, DateTime od, List<string> GL, List<Screening> SL)
        {
            Title = t;
            Duration = d;
            Classification = c;
            OpeningDate = od;
            GenreList = GL;
            ScreeningList = SL;
        }

        public void AddGenre(string g)
        {
            GenreList.Add(g);
        }

        public void AddScreening(Screening SC)
        {
            ScreeningList.Add(SC);
        }

        public override string ToString()
        {
            return "Title: " + Title + "\tDuration: " + Duration + "\tClassification: " + Classification + "\tOpening Date: " + OpeningDate + "\tGenre List: " + GenreList + "\tScreening List" + ScreeningList;
        }

    }
}
